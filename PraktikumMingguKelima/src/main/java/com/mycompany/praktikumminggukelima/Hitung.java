/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.praktikumminggukelima;
import java.util.Scanner;
/**
 *
 * @author HP
 */
public class Hitung {
    int nilai1;
    int nilai2;
    void methodTambah() {
        System.out.println( nilai1 + " + " + nilai2 + " = " + (nilai1 + nilai2));
    }
    void methodKurang() {
        System.out.println( nilai1 + " - " + nilai2 + " = " + (nilai1 - nilai2));
    }
    void methodKali() {
        System.out.println( nilai1 + " x " + nilai2 + " = " + (nilai1 * nilai2));
    }
    void methodBagi() {
        System.out.println( nilai1 + " : " + nilai2 + " = " + (nilai1 / nilai2));
    }
    void methodModulo() {
        System.out.println( nilai1 + " % " + nilai2 + " = " + (nilai1 % nilai2));
    }
    
    void input() {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai pertama : ");
        nilai1 = in.nextInt();
        System.out.print("Masukkan nilai kedua : ");
        nilai2 = in.nextInt();
    }
    
    void output() {
        System.out.println("Hasil Perhitungan : ");
        methodTambah();
        methodKurang();
        methodKali();
        methodBagi();
        methodModulo();
    }
    public static void main(String[] args) {
        Hitung h = new Hitung();
        h.input();
        h.output();
    }
}
