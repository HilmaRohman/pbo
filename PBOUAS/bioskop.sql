-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2023 at 05:07 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bioskop`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemesanan`
--

CREATE TABLE `detail_pemesanan` (
  `id_pemesanan` varchar(20) NOT NULL,
  `id_jam_tayang` varchar(20) NOT NULL,
  `nomor_kursi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `detail_pemesanan`
--

INSERT INTO `detail_pemesanan` (`id_pemesanan`, `id_jam_tayang`, `nomor_kursi`) VALUES
('T0001', 'J0001', 'B1'),
('T0001', 'J0001', 'B2');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_film`
--

CREATE TABLE `jadwal_film` (
  `id_film` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `total_kursi` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jadwal_film`
--

INSERT INTO `jadwal_film` (`id_film`, `nama`, `total_kursi`, `harga`) VALUES
('F0001', 'Fast and Furious 6', 80, 40000);

-- --------------------------------------------------------

--
-- Table structure for table `jam_tayang`
--

CREATE TABLE `jam_tayang` (
  `id_jam_tayang` varchar(20) NOT NULL,
  `id_film` varchar(20) NOT NULL,
  `jam_tayang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jam_tayang`
--

INSERT INTO `jam_tayang` (`id_jam_tayang`, `id_film`, `jam_tayang`) VALUES
('J0001', 'F0001', 'Malam - 22.00'),
('J0002', 'J0001', 'Sore - 15.00');

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `id_transaksi` char(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama_film` varchar(50) NOT NULL,
  `jam_tayang` varchar(20) NOT NULL,
  `jumlah_kursi` int(11) NOT NULL,
  `seat_kursi` char(3) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`id_transaksi`, `username`, `nama_film`, `jam_tayang`, `jumlah_kursi`, `seat_kursi`, `harga`) VALUES
('003', 'jojo', 'Tenggelamnya kapal Van der wijck', 'Malam', 2, 'B', 80000),
('006', 'fadlil', 'My Stupid boss', 'Pagi', 8, 'D', 320000),
('007', 'hilma', 'Habibie & Ainun', 'Pagi', 2, 'B', 80000),
('009', 'fadlil', 'Fast and Furious 6', 'Pagi', 4, 'A', 160000),
('010', 'jojo', 'Fast and Furious 6', 'Malam', 2, 'C', 80000),
('011', 'jojo', 'Extraction 2', 'Siang', 2, 'B', 80000),
('012', 'jojo', 'Gundala', 'Sore', 4, 'A', 160000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_pemesanan`
--

CREATE TABLE `transaksi_pemesanan` (
  `id_pemesanan` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `log_transaksi` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaksi_pemesanan`
--

INSERT INTO `transaksi_pemesanan` (`id_pemesanan`, `username`, `harga`, `log_transaksi`) VALUES
('T0001', 'jojo', 40000, '2023-07-13 13:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('fadlil', '0302'),
('hilma', '2910'),
('jojo', '1710');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jam_tayang`
--
ALTER TABLE `jam_tayang`
  ADD PRIMARY KEY (`id_jam_tayang`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_pemesanan`
--
ALTER TABLE `transaksi_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
