package PraktikumMingguKeempat.Latihan04d;
import java.util.Scanner;


public class Latihan04d {
    public static void main(String[] args) {
        String nama;
        String nopel;
        Integer pemakaian;

        Scanner scan = new Scanner(System.in);
        Air air = new Air();

        System.out.println("Perhitungan Biaya Pemakaian Air");
        System.out.println("===============================");
        
        System.out.print("Nama\t\t: ");
        nama = scan.nextLine();
        air.setNama(nama);
        
        System.out.print("No. Pelanggan\t: ");
        nopel = scan.nextLine();
        air.setNopel(nopel);
        
        System.out.print("Pemakaian Air\t: ");
        pemakaian = scan.nextInt();
        air.setPemakaian(pemakaian);
        
        air.bayar();
        System.out.println("Biaya Pakai\t: " + air.biaya);
        System.out.println("===============================");
    }
}