package PraktikumMingguKeempat.Latihan04d;

public class Tagihan {
    private String nama;
    private String nopel;
    
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setNopel(String nopel) {
        this.nopel = nopel;
    }
}

class Air extends Tagihan {
    private Integer pemakaian;
    protected Integer biaya = 0;
    public void setPemakaian(Integer pemakaian) {
        this.pemakaian = pemakaian;
    }

    void bayar () {
        for(Integer x = 1; x <= pemakaian; x++) {
            biaya += (x <= 10) ? 1000 : 0;
            biaya += (x > 10 && x <= 20) ? 2000 : 0;
            biaya += (x > 20) ? 5000 : 0;
        }
    }
}
