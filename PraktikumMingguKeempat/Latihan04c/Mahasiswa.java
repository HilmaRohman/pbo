package PraktikumMingguKeempat.Latihan04c;

public class Mahasiswa {
    private String nama;
    private String prodi;

    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setProdi(String prodi) {
        this.prodi = prodi;
    }
}

class KartuStudi extends Mahasiswa{
    Integer nilai;
    char nilaiHuruf;
    
    public void setNilai(Integer nilai) {
        this.nilai = nilai;
    }
    void kategori() {
        if (nilai <= 100 && nilai >= 85) {
            nilaiHuruf = 'A';
        } else if (nilai >= 70 && nilai < 85) {
            nilaiHuruf = 'B';
        } else if (nilai >= 60 && nilai < 70) {
            nilaiHuruf = 'C';
        } else if (nilai >= 50 && nilai < 60) {
            nilaiHuruf = 'D';
        } else if (nilai >= 0 && nilai < 50){
            nilaiHuruf = 'E';
        } else {
            nilaiHuruf = ' ';
        }
    }
}
