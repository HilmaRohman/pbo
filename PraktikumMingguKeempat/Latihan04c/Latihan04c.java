package PraktikumMingguKeempat.Latihan04c;
import java.util.Scanner;


public class Latihan04c {
    public static void main(String[] args) {
        String nama;
        String prodi;
        Integer nilai;

        Scanner scan = new Scanner(System.in);
        KartuStudi ks = new KartuStudi();
        
        System.out.println("Data Test");
        System.out.println("====================================");
        
        System.out.print("Nama\t\t: ");
        nama = scan.nextLine();
        ks.setNama(nama);
        
        System.out.print("Program Studi\t: ");
        prodi = scan.nextLine();
        ks.setProdi(prodi);
        
        System.out.print("Nilai\t\t: ");
        nilai = scan.nextInt();
        ks.setNilai(nilai);
        
        ks.kategori();
        System.out.println("Nilai Huruf\t: " + ks.nilaiHuruf);
        System.out.println("====================================");

    }
}