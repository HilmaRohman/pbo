package com.mycompany.week9.hilma;


public class CellphoneMain {
    public static void main(String[] args){
        
        Cellphone cp = new Cellphone("Nokia", "3310");
        
        cp.powerOn();
        
        cp.setPulsa(5000);
        cp.getPulsa();
        
        cp.setKontak("085156829510", "M. Hilma Minanur Rohman");
        cp.setKontak("085226567800", "Jossac Nur Juwahir");
        cp.allKontak();
        cp.getKontak("M. Hilma Minanur Rohman");
        
        cp.powerOff();
    }
}
