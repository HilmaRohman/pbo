package com.mycompany.week9.hilma;
import java.lang.Math;
import java.util.LinkedHashMap;
import java.util.Map;
import phone.Phone;

public class Cellphone implements Phone{
    String merk;
    String type;
    int batteryLevel;
    int status;
    int volume;
    int pulsa;
    int totKontak;
    Map<String, String> kontak = new LinkedHashMap<>();

    public Cellphone(String merk,String type)
    {
        this.merk = merk;
        this.type = type;
        this.batteryLevel = (int)(Math.random()*(100-0+1)+0);  
    }

    public void powerOn(){
        this.status = 1;
        System.out.println("Ponsel menyala");
        System.out.println("Sisa baterai: " + this.batteryLevel + "\n");
    }
    
    public void powerOff(){
        this.status = 0;
        System.out.println("Ponsel mati");
    }

    public void volumeUp(){
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat menaikkan volume");
        }
        else
        {
            this.volume++;
            if(this.volume>=100)
            {
                this.volume = 100;
            }
        }
    }

    public void volumeDown(){
        this.volume--;
        
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat menurunkan volume");
        }
        else
        {
            this.volume--;
            if(this.volume<=0)
            {
                this.volume = 0;
            }
        }
    }

    public int getVolume(){
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat melihat volume");
            return 0;
        }
        else
        {
            System.out.println("Volume saat ini: " + this.volume);
            return this.volume;
        }
    }
    
    public void setPulsa(int pulsa) {
        
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat mengisi volume");
        }
        else
        {
           this.pulsa += pulsa;
            System.out.println("Pulsa bertambah " + pulsa + " Sisa pulsa: " + this.pulsa); 
        }
    }
    
    public void getPulsa() {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat melihat sisa pulsa");
        }
        else
        {
            System.out.println("Sisa pulsa: " + this.pulsa);
        }
        
    }
    
    public void setKontak(String no, String nama) {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat mengedit kontak");
        }
        else
        {
            kontak.put(no, nama);
            System.out.println("Kontak dengan nama: " + nama + " dan nomor: " + no + " telah disimpan");
        }
    }
    
    public void allKontak() {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat melihat kontak");
        }
        else
        {
            System.out.println("Daftar Kontak: ");
            for (Map.Entry<String, String> k : kontak.entrySet()){
                System.out.println("Nama: " + k.getKey());
                System.out.println("Nomor: " + k.getValue() + "\n");
            }
        }
        
    }    
    public void getKontak(String nama) {
        if(this.status == 0){
            System.out.println("Ponsel mati. Tidak dapat mencari kontak");
        }
        else
        {
            boolean ketemu = false;
            for (Map.Entry<String, String> k : kontak.entrySet()){
                if(k.getValue() == nama){
                    ketemu = true;
                    System.out.println("Pencarian kontak " + k.getValue() + " ketemu");
                    System.out.println("Nama : " + k.getValue());
                    System.out.println("Nomor: " + k.getKey());
                }
            }

            if(ketemu == false) {
                System.out.println("Data kontak tidak ditemukan");
            }
        }
        
    }   
}
